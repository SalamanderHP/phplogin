<?php
require_once "modal.php";

$modal_obj=new DB_Modal();
    //xu ly dang nhap
    if(isset($_POST['submit']))
    {
        $data = $modal_obj->db_select();
        //truyen text vao db de db thuc hien ham
        while($dbvalue = mysqli_fetch_assoc($data))
        {
            if($_POST['email']==$dbvalue['email']&&$_POST['password']==$dbvalue['password']){
                return require_once "dashboard_form.php";
            }
        }
        return require_once "error_form.php";
    }

    //xu ly dang ky
    elseif(isset($_POST['register'])){
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm = $_POST['confirm'];
        if($password==$confirm){
            $modal_obj -> db_insert($username,$email,$password);
            sleep(2);
            return require_once "login_form.php";
        }
        else{
            echo("Password must be confirm correctly");
        }
    }

    //xu ly dang xuat, xoa tai khoan
    elseif(isset($_POST['logout'])){
        return require_once "login_form.php";
    }
    elseif (isset($_POST['delete'])) {
        $modal_obj -> remove($_POST['delete']);
        sleep(1);
        return require_once "login_form.php";
    }

    else
    {
        return require_once "login_form.php";
    }
?>